package main

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)

// product represents an entity that mirrors the date coming from garage product server
type product struct {
	Name   string  `json:"name"`
	Price  float64 `json:"price"`
	Amount int     `json:"amount"`
	Unit   string  `json:"unit"`
	Stock  uint    `json:"stock"`
}

// response represents the generic product listing response from garage product server
type response struct {
	Error    bool      `json:"Error"`
	Message  string    `json:"Message"`
	Products []product `json:"Products"`
}

// registerResponse represents the register response from garage product server
// The ID belongs to a database on the garage product server, it is needed for other request as auth header
type registerResponse struct {
	Error bool   `json:"error"`
	ID    string `json:"id"`
}

func main() {

	// First we need to know which catalog to work with
	responseStruct := registerIfNeeded()
	catalog := responseStruct.ID

	//TODO Old code from here, rewrite for new API

	list := ask("Do you want to see the listing? (Yes/no)\n> ")
	list = strings.ToLower(list)
	if list != "yes" && list != "y" && list != "" {
		fmt.Println("See you next time!")
		return
	}

	products := getProducts(catalog)

	displayProducts(products, true)

	userChoice := ask("Which cheese do you want? > ")

	products, err := buyProduct(products, userChoice)
	displayProducts(products, false)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Printf("You bought 1 %q.\n", userChoice)
	}
}

// registerIfNeeded checks if a config.json exists and if not registers with the sevrer and creates one
// In either case, a registerResponse with a database ID is returned
func registerIfNeeded() registerResponse {

	responseStruct := registerResponse{}

	if _, err := os.Stat("config.json"); os.IsNotExist(err) {
		resp, err := http.Post("http://mc.3stadt.com:8008/register", "application/json", nil)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		data, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		err = json.Unmarshal(data, &responseStruct)
		if err != nil {
			fmt.Println("[ERROR] could not unmarshal json:", err)
			fmt.Printf("\n---------\n%s\n---------\n", data)
			os.Exit(1)
		}

		ioutil.WriteFile("config.json", data, 0666)

		return responseStruct
	}

	data, err := ioutil.ReadFile("config.json")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	err = json.Unmarshal(data, &responseStruct)
	if err != nil {
		fmt.Println("[ERROR] could not unmarshal json:", err)
		fmt.Printf("\n---------\n%s\n---------\n", data)
		os.Exit(1)
	}

	//FIXME check if id is valid on the garage product server
	return responseStruct
}

func ask(question string) string {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print(question)
	userChoice, _ := reader.ReadString('\n')
	userChoice = strings.TrimSpace(userChoice) // remove the newline from user input
	return userChoice
}

func getProducts(catalog string) []product {
	resp, err := http.Get("http://mc.3stadt.com:8008/catalog/" + catalog)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	responseStruct := response{}
	err = json.Unmarshal(data, &responseStruct)
	if err != nil {
		fmt.Println("[ERROR] could not unmarshal json:", err)
		fmt.Printf("\n---------\n%s\n---------\n", data)
		os.Exit(1)
	}

	if responseStruct.Error == true {
		fmt.Println("No products available.")
		fmt.Println(responseStruct.Message)
		os.Exit(1)
	}

	return responseStruct.Products
}

func buyProduct(products []product, name string) ([]product, error) {
	for i := 0; i < len(products); i++ {
		if products[i].Name == name {
			if products[i].Stock > 0 {
				products[i].Stock--
				return products, nil
			}
			return products, errors.New("The product is not available")
		}
	}
	return products, errors.New("The product does not exist")
}

func displayProducts(products []product, short bool) {
	for _, product := range products {
		if short {
			fmt.Printf("%s: %.2f\n", product.Name, product.Price)
			continue
		}
		fmt.Printf(`Product Name: %s
Price: %.2f
Amount: %d %s
Available: %t
DEBUG %d
---
`, product.Name, product.Price, product.Amount, product.Unit, product.Stock > 0, product.Stock)
	}
}
